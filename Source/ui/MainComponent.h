/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/
/** MainComponent Class @see*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "FilePlayerGui.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel



                       // public Counter::Listener

{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);

    /** Destructor */
    ~MainComponent();
    
    /** Resize */
    void resized() override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
   // void sliderValueChanged(Slider* slider);
    
   
   
    
private:
    Audio& audio;
    
     /** FilePlayerGUI declaration */
     FilePlayerGui filePlayerGui;
     FilePlayerGui filePlayerGui2;
     FilePlayerGui filePlayerGui3;
     FilePlayerGui filePlayerGui4;
   
    
    /** FilePlayer declaration */
    FilePlayer filePlayer;
    FilePlayer filePlayer2;
    FilePlayer filePlayer3;
    FilePlayer filePlayer4;
    
    
    
  
    /** UI Labels */
    Label kickLabel;
    Label snareLabel;
    Label hatLabel;
    Label rideLabel;
   
    
    

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
