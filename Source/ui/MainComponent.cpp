/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

/** Basic Drum-Machine @see */

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_), filePlayerGui(audio_.getFilePlayer()), filePlayerGui2(audio_.getFilePlayer2()), filePlayerGui3(audio_.getFilePlayer3()), filePlayerGui4(audio_.getFilePlayer4())
{
    setSize (300, 280);
    addAndMakeVisible(&filePlayerGui);
    addAndMakeVisible(&filePlayerGui2);
    addAndMakeVisible(&filePlayerGui3);
    addAndMakeVisible(&filePlayerGui4); 
  
   
    
    
    kickLabel.setText("Kick", dontSendNotification);
    addAndMakeVisible(&kickLabel);
    
    snareLabel.setText("Snare", dontSendNotification);
    addAndMakeVisible(&snareLabel);
    
    hatLabel.setText("Hat", dontSendNotification);
    addAndMakeVisible(&hatLabel);
    
    rideLabel.setText("Ride", dontSendNotification);
    addAndMakeVisible(&rideLabel);
    
    
   
}

MainComponent::~MainComponent()
{
   // mixerAudioSource.removeAllInputs();
    
   
}

void MainComponent::resized()
{
    filePlayerGui.setBounds (7, 0, getWidth(), getHeight());
    kickLabel.setBounds(14,200,50,50);
    
    filePlayerGui2.setBounds(getWidth()/4, 0, getWidth(), getHeight());
    snareLabel.setBounds(getWidth()/4 + 4,200,50,50);
    
    filePlayerGui3.setBounds(getWidth()/2, 0, getWidth(), getHeight());
    hatLabel.setBounds(getWidth()/2 + 10,200,50,50);
    
    filePlayerGui4.setBounds((getWidth()/4) * 3, 0, getWidth(), getHeight());
    rideLabel.setBounds(((getWidth()/4) * 3) + 7,200,50,50);
    
   
    
    
    //counter1.setBounds(0, 200, 70, 70);
   // masterGainSlider.setBounds(200, 200, 70, 70);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
