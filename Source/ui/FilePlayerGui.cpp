
#include "FilePlayerGui.h"
#include "Audio.h"

/** File Player GUI  */

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_), filePlayer2(filePlayer2), filePlayer3(filePlayer3), filePlayer4(filePlayer4) //pass each file player address into the correct filePlayerGui
{
   
    /** Play Button attributes */
    playButton.setButtonText (">");
    playButton.addListener(this);
    playButton.setColour(TextButton::buttonColourId, Colours::white);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight | Button::ConnectedOnTop | Button:: ConnectedOnBottom);
    addAndMakeVisible(&playButton);
    
    
    /** Pitch Slider attributes */
    pitchSlider.setSliderStyle(Slider::Rotary);
    pitchSlider.setRange(0.01, 5.0);
    pitchSlider.setValue(1.0);
    pitchSlider.addListener(this);
    pitchSlider.setColour(Slider::rotarySliderOutlineColourId, Colours::white);
    pitchSlider.setColour(Slider::rotarySliderFillColourId, Colours::white);
    pitchSlider.setColour(Slider::textBoxBackgroundColourId, Colours::white);
    pitchSlider.setColour(Slider::textBoxOutlineColourId, Colours::white);
    pitchSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 30, 15);
    addAndMakeVisible(&pitchSlider);
    
    /** Gain Slider attributes */
    gainSlider.setSliderStyle(Slider::LinearBarVertical);
    gainSlider.setRange(0.0, 1.0);
    gainSlider.setValue(0.5);
    gainSlider.addListener(this);
    gainSlider.setColour(Slider::thumbColourId, Colours::white);
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    addAndMakeVisible(&gainSlider);
    
    
    /** File Chooser Attributes*/
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    
    addAndMakeVisible(fileChooser);
    
    
}
/** Delete fileChooser when not in use */
FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}


/** Placement of UI components */
void FilePlayerGui::resized()
{
    
    fileChooser->setBounds (5, 10, 65, 15);
    pitchSlider.setBounds(0, 30, 50, 50);
    
    gainSlider.setBounds(11.5, 90, 30, 70);
    
    playButton.setBounds (11.5, 170, 30, 30);

}

//** Button Listener */
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
    }
}

//** File Name Component Listener */
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}
//** Slider Listener */
void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    
    if (slider == &pitchSlider)
    {
        pos = pitchSlider.getValue(); //** Read value from pitch slider*/
        filePlayer.setPlaybackRate(pos); //** Use value from slider to control the playback rate*/
        DBG(pos);
        
    }
    else if (slider == &gainSlider)
    {
        pos = gainSlider.getValue(); //** Read value from gain slider*/
        filePlayer.setGain(pos); //** Use value from slider to control gain*/
    }

}