
#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

class Audio : public Component,
              public Button::Listener,
              public MidiInputCallback,
              public AudioIODeviceCallback




{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    void resized();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    FilePlayer& getFilePlayer() { return filePlayer; }
    FilePlayer& getFilePlayer2() { return filePlayer2; }
    FilePlayer& getFilePlayer3() { return filePlayer3; }
    FilePlayer& getFilePlayer4() { return filePlayer4; }
    
    /** Handles Midi */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    void buttonClicked(Button* button) override;
   
private:
    
   
    
    AudioDeviceManager audioDeviceManager;
  
    
    AudioSourcePlayer audioSourcePlayer;
    
   
    FilePlayer filePlayer;
    FilePlayer filePlayer2;
    FilePlayer filePlayer3;
    FilePlayer filePlayer4;
    
    TextButton mLButton1;
    TextButton mLButton2;
    TextButton mLButton3;
    TextButton mLButton4;
    
    
    MixerAudioSource mixerAudioSource;
    
    
    float pos;
    
    MidiMessage midiMessage;
    
    float mLNote1;
    float mLNote2;
    float mLNote3;
    float mLNote4;
};



#endif  // AUDIO_H_INCLUDED
