
#ifndef H_FILEPLAYERGUI
#define H_FILEPLAYERGUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"


/**
 Gui for the FilePlayer class
 */
class FilePlayerGui :   public Component,
                        public Button::Listener,
                        public FilenameComponentListener,
                        public Slider::Listener

{
public:
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    FilePlayerGui(FilePlayer& filePlayer_);
  
    
    /**
     Destructor 
     */
    ~FilePlayerGui();
    
    //** Component */
    void resized();
    
    //** Button Listener */
    void buttonClicked (Button* button);
    
    //** Filename Component Listener */
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged);
    
    void sliderValueChanged(Slider* slider) override;
    

private:
    TextButton playButton;
    FilenameComponent* fileChooser;
    
    FilePlayer& filePlayer;
    FilePlayer& filePlayer2;
    FilePlayer& filePlayer3;
    FilePlayer& filePlayer4;
    
    Slider playbackPosSlider;
    Slider pitchSlider;
    Slider gainSlider;
    Slider distSlider;
    
   
    
    
    
    float pos; /** variable for slider position */
    
   
};


#endif  // H_FILEPLAYERGUI
