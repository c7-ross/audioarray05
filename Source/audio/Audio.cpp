

#include "Audio.h"

Audio::Audio()
{
   
        mixerAudioSource.addInputSource(&filePlayer, true);  /** Add each filePlayer to the mixer source */
        mixerAudioSource.addInputSource(&filePlayer2, true);
        mixerAudioSource.addInputSource(&filePlayer3, true);
        mixerAudioSource.addInputSource(&filePlayer4, true);
    
    
     audioDeviceManager.setMidiInputEnabled("Nocturn Keyboard", true);
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); /** 2 inputs, 2 outputs */
    
    /** load the mixerAudioSource as the source for the audio player */
    audioSourcePlayer.setSource (&mixerAudioSource);
   
   
    /** Add Midi & Audio callbacks */
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    mLButton1.addListener(this);
    addAndMakeVisible(&mLButton1);
    
    addAndMakeVisible(&mLButton2);
    addAndMakeVisible(&mLButton3);
    addAndMakeVisible(&mLButton4);
       
}

Audio::~Audio()
{
    /** remove sources when not in use */
    
    mixerAudioSource.removeInputSource(&filePlayer);
    mixerAudioSource.removeInputSource(&filePlayer2);
    mixerAudioSource.removeInputSource(&filePlayer3);
    mixerAudioSource.removeInputSource(&filePlayer4);
    
    audioDeviceManager.removeAudioCallback (this);
    
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}
void Audio::resized()
{
    mLButton1.setBounds(40, 40, 40,40);
}

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    DBG("Midi recived");
    DBG("Midi Note Number: " << message.getNoteNumber());
    
    
    
     if (message.getNoteNumber() == 48)
    {
        if (message.isNoteOn() == true) /** Done to avoid the sample replaying on the after-touch */
        {
            filePlayer.setPlaying(!filePlayer.isPlaying()); /** Assign filePlayer1 to Midi note number 48*/
        }
    }
     else if (message.getNoteNumber() == 50)
    {
        if (message.isNoteOn() == true)
        {
            filePlayer2.setPlaying(!filePlayer2.isPlaying()); /** Assign filePlayer2 to Midi note number 50*/
        }
      
    }
   else if (message.getNoteNumber() == 52 )
    {
        if (message.isNoteOn() == true)
        {
            filePlayer3.setPlaying(!filePlayer3.isPlaying()); /** Assign filePlayer3 to Midi note number 52*/

        }
    }
    else if (message.getNoteNumber() == 53)
    {
        if (message.isNoteOn() == true)
        {
            filePlayer4.setPlaying(!filePlayer4.isPlaying()); /** Assign filePlayer4 to Midi note number 53*/

        }
    }
}


void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
  
    
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    
    
    
    
    
    float inSampL;
    float inSampR;
    
    while(numSamples--)
    {
        inSampL = *outL;
        inSampR = *outL;
        
        
        *outL = inSampL * 1.f;
        *outR = inSampR * 1.f;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
   
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
    
}

/** Midi Learn test function */

void Audio::buttonClicked(Button* button)
{
    if (button == &mLButton1)
    {
        DBG("Play a note");
        mLNote1 = midiMessage.getNoteNumber();
        DBG(mLNote1);
    }
    else if (button == &mLButton2)
    {
        DBG("Play a note");
        mLNote2 = midiMessage.getNoteNumber();
        DBG(mLNote2);
    }
    else if (button == &mLButton3)
    {
        DBG("Play a note");
        mLNote3 = midiMessage.getNoteNumber();
        DBG(mLNote3);
    }
    else if (button == &mLButton4)
    {
        DBG("Play a note");
        mLNote4 = midiMessage.getNoteNumber();
        DBG(mLNote4);
    }


}